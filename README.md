The final exercise of the 2 week EPAM's Java training.

There is a postman export at fina-exercise/final-exercise.postman_collection.json! 
It can be used to test the program. Just need one environment property named "url" with value: localhost:8080

1. use the createuser to create a user
2. use the createcluster to create a cluster
3. use /cluster/assignUserToCluster to assign cluster to user
4. use create server to create a server
5. use addServerToCluster to add the created server to cluster
6. use sendCommand to send a command to the server
7. use getSentCommands to check on already sent valid commands
.
.
... there are more commands to check if everything is fine.

Hope everything works fine!
package com.java.training.finalexercise.converter.cluster;

import com.java.training.finalexercise.converter.server.ServerToServerDTOConverter;
import com.java.training.finalexercise.converter.user.UserToUserDTOWithoutClustersConverter;
import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.dto.cluster.ClusterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ClusterToClusterDTOConverter implements Converter<Cluster, ClusterDTO> {

    private ServerToServerDTOConverter serverToServerDTOConverter;
    private UserToUserDTOWithoutClustersConverter userToUserDTOWithoutClustersConverter;

    @Autowired
    public ClusterToClusterDTOConverter(ServerToServerDTOConverter serverToServerDTOConverter,
                                        UserToUserDTOWithoutClustersConverter userToUserDTOWithoutClustersConverter) {
        this.serverToServerDTOConverter = serverToServerDTOConverter;
        this.userToUserDTOWithoutClustersConverter = userToUserDTOWithoutClustersConverter;
    }

    @Override
    public ClusterDTO convert(Cluster cluster) {
        return ClusterDTO.Builder.aClusterDTO()
                .withId(cluster.getId())
                .withName(cluster.getName())
                .withServers(
                        cluster.getServers().stream()
                                .map(serverToServerDTOConverter::convert)
                                .collect(Collectors.toList())
                )
                .withUsers(cluster.getUsers().stream().map(userToUserDTOWithoutClustersConverter::convert).collect(Collectors.toSet()))
                .build();
    }
}

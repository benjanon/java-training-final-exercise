package com.java.training.finalexercise.dto.user;

public class UserDTOWithoutClusters {
    private int id;
    private String name;
    private boolean isDeactivated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeactivated() {
        return isDeactivated;
    }

    public void setDeactivated(boolean deactivated) {
        isDeactivated = deactivated;
    }

    public static class Builder{
        private int id;
        private String name;
        private boolean isDeactivated;

        private Builder(){}

        public static Builder aUserDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withDeactivated(boolean isDactivated){
            this.isDeactivated = isDactivated;
            return this;
        }

        public UserDTOWithoutClusters build(){
            UserDTOWithoutClusters user = new UserDTOWithoutClusters();
            user.setId(this.id);
            user.setName(this.name);
            user.setDeactivated(this.isDeactivated);

            return user;
        }
    }
}

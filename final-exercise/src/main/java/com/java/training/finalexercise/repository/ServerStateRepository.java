package com.java.training.finalexercise.repository;

import com.java.training.finalexercise.model.server.ServerState;
import org.springframework.data.repository.CrudRepository;

public interface ServerStateRepository extends CrudRepository<ServerState, Integer> {
}

package com.java.training.finalexercise.model.cluster;

import com.java.training.finalexercise.model.server.Server;
import com.java.training.finalexercise.model.user.User;

import javax.persistence.*;
import java.util.*;

@Entity
public class Cluster {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    @OneToMany
    private List<Server> servers = new ArrayList<>();

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "cluster_user",
            joinColumns = @JoinColumn(name = "cluster_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> users = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Server> getServers() {
        return servers;
    }

    public void setServers(List<Server> servers) {
        this.servers = servers;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cluster cluster = (Cluster) o;
        return id == cluster.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static class Builder{
        private int id;
        private String name;

        private List<Server> servers = new ArrayList<>();

        private Set<User> users = new HashSet<>();


        private Builder(){}

        public static Builder aCluster(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withUsers(Set<User> users){
            this.users = users;
            return this;
        }

        public Builder withServers(List<Server> servers){
            this.servers = servers;
            return this;
        }

        public Cluster build(){
            Cluster cluster = new Cluster();
            cluster.setId(this.id);
            cluster.setName(this.name);
            cluster.setServers(this.servers);
            cluster.setUsers(this.users);

            return cluster;
        }
    }
}

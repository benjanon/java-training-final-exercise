package com.java.training.finalexercise.repository;

import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.model.server.Server;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClusterRepository extends CrudRepository<Cluster, Integer> {
}

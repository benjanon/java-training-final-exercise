package com.java.training.finalexercise.converter.cluster;

import com.java.training.finalexercise.converter.server.ServerDTOToServerConverter;
import com.java.training.finalexercise.converter.user.UserDTOWithoutClustersToUserController;
import com.java.training.finalexercise.dto.cluster.ClusterDTO;
import com.java.training.finalexercise.model.cluster.Cluster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ClusterDTOToClusterConverter implements Converter<ClusterDTO, Cluster> {

    private ServerDTOToServerConverter serverDTOToServerConverter;
    private UserDTOWithoutClustersToUserController userDTOWithoutClustersToUserController;

    @Autowired
    public ClusterDTOToClusterConverter(ServerDTOToServerConverter serverDTOToServerConverter,
                                        UserDTOWithoutClustersToUserController userDTOWithoutClustersToUserController) {
        this.serverDTOToServerConverter = serverDTOToServerConverter;
        this.userDTOWithoutClustersToUserController = userDTOWithoutClustersToUserController;
    }

    @Override
    public Cluster convert(ClusterDTO clusterDTO) {
        return Cluster.Builder.aCluster()
                .withId(clusterDTO.getId())
                .withName(clusterDTO.getName())
                .withServers(clusterDTO.getServers().stream().map(serverDTOToServerConverter::convert).collect(Collectors.toList()))
                .withUsers(clusterDTO.getUsers().stream().map(userDTOWithoutClustersToUserController::convert).collect(Collectors.toSet()))
                .build();
    }
}

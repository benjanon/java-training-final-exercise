package com.java.training.finalexercise.dto.command;

import com.java.training.finalexercise.model.command.CommandType;
import com.java.training.finalexercise.dto.user.UserDTO;

import java.util.Date;
import java.util.Objects;

public class CommandDTO {
    private int id;

    private Date timeStamp = new Date();

    private UserDTO user;

    private CommandType commandType;

    private String process;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandDTO that = (CommandDTO) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public static class Builder{
        private int id;

        private Date timeStamp = new Date();

        private UserDTO user;

        private CommandType commandType;

        private String process;

        private Builder(){}

        public static Builder aCommandDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withTimeStamp(Date timeStamp){
            this.timeStamp = timeStamp;
            return this;
        }

        public Builder withUser(UserDTO user){
            this.user = user;
            return this;
        }

        public Builder withCommandType(CommandType commandType){
            this.commandType = commandType;
            return this;
        }

        public Builder withProcess(String process){
            this.process = process;
            return this;
        }


        public CommandDTO build(){
            CommandDTO command = new CommandDTO();
            command.setId(this.id);
            command.setTimeStamp(this.timeStamp);
            command.setUser(this.user);
            command.setCommandType(this.commandType);
            command.setProcess(this.process);

            return command;
        }
    }
}

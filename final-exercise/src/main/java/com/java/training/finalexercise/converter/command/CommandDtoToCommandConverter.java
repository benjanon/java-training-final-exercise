package com.java.training.finalexercise.converter.command;

import com.java.training.finalexercise.converter.user.UserDTOToUserConverter;
import com.java.training.finalexercise.dto.command.CommandDTO;
import com.java.training.finalexercise.model.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommandDtoToCommandConverter implements Converter<CommandDTO,Command> {

    private UserDTOToUserConverter userDTOToUserConverter;

    @Autowired
    public CommandDtoToCommandConverter(UserDTOToUserConverter userDTOToUserConverter) {
        this.userDTOToUserConverter = userDTOToUserConverter;
    }

    @Override
    public Command convert(CommandDTO commandDTO) {
        return Command.Builder.aCommand()
                .withId(commandDTO.getId())
                .withCommandType(commandDTO.getCommandType())
                .withProcess(commandDTO.getProcess())
                .withTimeStamp(commandDTO.getTimeStamp())
                .withUser(userDTOToUserConverter.convert(commandDTO.getUser()))
                .build();
    }
}

package com.java.training.finalexercise.converter.user;

import com.java.training.finalexercise.dto.user.UserDTOWithoutClusters;
import com.java.training.finalexercise.model.user.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDTOWithoutClustersToUserController implements Converter<UserDTOWithoutClusters, User> {
    @Override
    public User convert(UserDTOWithoutClusters userDTOWithoutClusters) {
        return User.Builder.aUser()
                .withId(userDTOWithoutClusters.getId())
                .withName(userDTOWithoutClusters.getName())
                .withDeactivated(userDTOWithoutClusters.isDeactivated())
                .build();
    }
}

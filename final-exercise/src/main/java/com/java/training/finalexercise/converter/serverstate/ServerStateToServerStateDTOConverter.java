package com.java.training.finalexercise.converter.serverstate;

import com.java.training.finalexercise.dto.server.ServerStateDTO;
import com.java.training.finalexercise.model.server.ServerState;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ServerStateToServerStateDTOConverter implements Converter<ServerState, ServerStateDTO> {


    @Override
    public ServerStateDTO convert(ServerState serverState) {
        return ServerStateDTO.Builder.aServerStateDTO()
                .withId(serverState.getId())
                .withProcess(serverState.getProcess())
                .withState(serverState.getState())
                .withTimeStamp(serverState.getTimeStamp())
                .build();
    }
}

package com.java.training.finalexercise.service;

import com.java.training.finalexercise.converter.cluster.ClusterDTOToClusterConverter;
import com.java.training.finalexercise.converter.cluster.ClusterToClusterDTOConverter;
import com.java.training.finalexercise.dto.cluster.ClusterDTO;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.model.user.User;
import com.java.training.finalexercise.repository.ClusterRepository;
import com.java.training.finalexercise.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ClusterService {

    private ClusterRepository clusterRepository;
    private UserRepository userRepository;
    private ClusterToClusterDTOConverter clusterToClusterDTOConverter;
    private ClusterDTOToClusterConverter clusterDTOToClusterConverter;
    private UtilsService utilsService;

    @Autowired
    public ClusterService(ClusterRepository clusterRepository,
                          UserRepository userRepository, ClusterToClusterDTOConverter clusterToClusterDTOConverter,
                          ClusterDTOToClusterConverter clusterDTOToClusterConverter,
                          UtilsService utilsService) {
        this.clusterRepository = clusterRepository;
        this.userRepository = userRepository;
        this.clusterToClusterDTOConverter = clusterToClusterDTOConverter;
        this.clusterDTOToClusterConverter = clusterDTOToClusterConverter;
        this.utilsService = utilsService;
    }

    public ClusterDTO createCluster(ClusterDTO cluster){
        return clusterToClusterDTOConverter.convert(clusterRepository.save(clusterDTOToClusterConverter.convert(cluster)));
    }

    public ClusterDTO updateCluster(ClusterDTO cluster){
        return clusterToClusterDTOConverter.convert(clusterRepository.save(clusterDTOToClusterConverter.convert(cluster)));
    }

    public ClusterDTO getCluster(int id){
        Cluster c = clusterRepository.findById(id).orElse(null);
        return Objects.isNull(c) ? null : clusterToClusterDTOConverter.convert(c);
    }

    public List<ClusterDTO> getClusters(){
        return utilsService.toList(clusterRepository.findAll()).stream().map(clusterToClusterDTOConverter::convert).collect(Collectors.toList());
    }

    public ClusterDTO assignUserToCluster(UserDTO userDTO, ClusterDTO clusterDTO){
        Cluster cluster = clusterRepository.findById(clusterDTO.getId()).orElse(null);
        User user = userRepository.findById(userDTO.getId()).orElse(null);
        cluster.getUsers().add(user);
        return clusterToClusterDTOConverter.convert(clusterRepository.save(cluster));
    }

    public void unassignUserFromCluster(User user, Cluster cluster){
        cluster.getUsers().remove(user);
        //user.getClusters().remove(cluster);
    }
}

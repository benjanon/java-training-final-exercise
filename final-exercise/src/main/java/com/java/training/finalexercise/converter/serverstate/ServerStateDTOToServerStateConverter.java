package com.java.training.finalexercise.converter.serverstate;

import com.java.training.finalexercise.dto.server.ServerStateDTO;
import com.java.training.finalexercise.model.server.ServerState;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ServerStateDTOToServerStateConverter implements Converter<ServerStateDTO, ServerState> {

    @Override
    public ServerState convert(ServerStateDTO serverStateDTO) {
        return ServerState.Builder.aServerState()
                .withId(serverStateDTO.getId())
                .withProcess(serverStateDTO.getProcess())
                .withState(serverStateDTO.getState())
                .withTimeStamp(serverStateDTO.getTimeStamp())
                .build();
    }
}

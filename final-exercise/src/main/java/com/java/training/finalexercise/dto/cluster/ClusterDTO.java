package com.java.training.finalexercise.dto.cluster;

import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.dto.user.UserDTOWithoutClusters;

import java.util.*;

public class ClusterDTO {

    private int id;
    private String name;

    private List<ServerDTO> servers = new ArrayList<>();

    private Set<UserDTOWithoutClusters> users = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServerDTO> getServers() {
        return servers;
    }

    public void setServers(List<ServerDTO> servers) {
        this.servers = servers;
    }

    public Set<UserDTOWithoutClusters> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTOWithoutClusters> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClusterDTO that = (ClusterDTO) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public static class Builder{
        private int id;
        private String name;

        private List<ServerDTO> servers = new ArrayList<>();

        private Set<UserDTOWithoutClusters> users = new HashSet<>();


        private Builder(){}

        public static Builder aClusterDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withUsers(Set<UserDTOWithoutClusters> users){
            this.users = users;
            return this;
        }

        public Builder withServers(List<ServerDTO> servers){
            this.servers = servers;
            return this;
        }

        public ClusterDTO build(){
            ClusterDTO cluster = new ClusterDTO();
            cluster.setId(this.id);
            cluster.setName(this.name);
            cluster.setServers(this.servers);
            cluster.setUsers(this.users);

            return cluster;
        }
    }
}

package com.java.training.finalexercise.model.command;

public enum CommandType {
    SHOTDOWN,
    STARTUP,
    RUNPROCESS,
    STOPPROCESS
}

package com.java.training.finalexercise.converter.server;

import com.java.training.finalexercise.converter.serverstate.ServerStateToServerStateDTOConverter;
import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.model.server.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ServerToServerDTOConverter implements Converter<Server, ServerDTO> {

    private ServerStateToServerStateDTOConverter serverStateToServerStateDTOConverter;

    @Autowired
    public ServerToServerDTOConverter(ServerStateToServerStateDTOConverter serverStateToServerStateDTOConverter) {
        this.serverStateToServerStateDTOConverter = serverStateToServerStateDTOConverter;
    }

    @Override
    public ServerDTO convert(Server server) {
        return ServerDTO.Builder.aServerDTO()
                .withId(server.getId())
                .withActualState(serverStateToServerStateDTOConverter.convert(server.getActualState()))
                .withCPU(server.getCpu())
                .withMemory(server.getMemory())
                .withIPAdress(server.getIpAddress())
                .withLogicalName(server.getLogicalName())
                .withPreviousStates(
                        server.getPreviousStates().stream()
                                .map(serverStateToServerStateDTOConverter::convert)
                                .collect(Collectors.toList())
                )
                .build();
    }
}

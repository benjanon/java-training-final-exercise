package com.java.training.finalexercise.controller;

import com.java.training.finalexercise.dto.cluster.ClusterDTO;
import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.model.server.Server;
import com.java.training.finalexercise.model.user.User;
import com.java.training.finalexercise.service.ClusterService;
import com.java.training.finalexercise.service.ServerService;
import com.java.training.finalexercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/cluster")
public class ClusterController {

    private ClusterService clusterService;
    private UserService userService;
    private ServerService serverService;

    @Autowired
    public ClusterController(ClusterService clusterService, UserService userService, ServerService serverService) {
        this.clusterService = clusterService;
        this.userService = userService;
        this.serverService = serverService;
    }

    @PostMapping("/create")
    public ResponseEntity<ClusterDTO> createCluster(@RequestBody ClusterDTO cluster){
        return new ResponseEntity<>(clusterService.createCluster(cluster), HttpStatus.OK);
    }

    @GetMapping("/cluster")
    public ResponseEntity<ClusterDTO> getCluster(@RequestParam("clusterId") int id){
        return new ResponseEntity<>(clusterService.getCluster(id), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<ClusterDTO>> getClusters(){
        return new ResponseEntity<>(clusterService.getClusters(), HttpStatus.OK);
    }

    @GetMapping("/assign")
    public ResponseEntity<ClusterDTO> assignUser(
            @RequestParam("clusterId") int clusterId,
            @RequestParam("userId") int userId){
        ClusterDTO cluster = clusterService.getCluster(clusterId);
        UserDTO user = userService.getUser(userId);
        if(Objects.isNull(cluster) || Objects.isNull(user)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(clusterService.assignUserToCluster(user, cluster), HttpStatus.OK);
    }

    @GetMapping("/server/add")
    public ResponseEntity<ClusterDTO> addServerToCluster(
            @RequestParam("clusterId") int clusterId,
            @RequestParam("serverId") int serverId){
        ClusterDTO cluster = clusterService.getCluster(clusterId);
        ServerDTO server = serverService.getServer(serverId);
        if(Objects.isNull(cluster) || Objects.isNull(server)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        cluster.getServers().add(server);
        return new ResponseEntity<>(clusterService.updateCluster(cluster), HttpStatus.OK);
    }

    @GetMapping("/server/remove")
    public ResponseEntity<ClusterDTO> removeServerFromCluster(
            @RequestParam("clusterId") int clusterId,
            @RequestParam("serverId") int serverId){
        ClusterDTO cluster = clusterService.getCluster(clusterId);
        ServerDTO server = serverService.getServer(serverId);
        if(Objects.isNull(cluster) || Objects.isNull(server)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(cluster.getServers().remove(server)) {
            return new ResponseEntity<>(clusterService.updateCluster(cluster), HttpStatus.OK);
        }
        return new ResponseEntity<>(cluster, HttpStatus.OK);
    }

    @GetMapping("/servers")
    public ResponseEntity<Iterable<ServerDTO>> getServersOfCluster(@RequestParam("clusterId") int clusterId){
        ClusterDTO cluster = clusterService.getCluster(clusterId);
        if(Objects.isNull(cluster)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cluster.getServers(), HttpStatus.OK);
    }
}

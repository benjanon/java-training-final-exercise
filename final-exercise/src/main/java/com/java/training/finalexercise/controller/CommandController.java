package com.java.training.finalexercise.controller;

import com.java.training.finalexercise.dto.cluster.ClusterDTO;
import com.java.training.finalexercise.dto.cluster.ClusterDTOWithoutUsers;
import com.java.training.finalexercise.dto.command.CommandDTO;
import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.model.command.Command;
import com.java.training.finalexercise.model.server.Server;
import com.java.training.finalexercise.model.user.User;
import com.java.training.finalexercise.service.ClusterService;
import com.java.training.finalexercise.service.CommandService;
import com.java.training.finalexercise.service.ServerService;
import com.java.training.finalexercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("command")
public class CommandController {

    private CommandService commandService;
    private UserService userService;
    private ServerService serverService;
    private ClusterService clusterService;

    @Autowired
    public CommandController(CommandService commandService, UserService userService, ServerService serverService, ClusterService clusterService) {
        this.commandService = commandService;
        this.userService = userService;
        this.serverService = serverService;
        this.clusterService = clusterService;
    }

    @PostMapping("/send")
    public ResponseEntity<CommandDTO> sendCommand(@RequestBody CommandDTO command,
                                                 @RequestParam("userId") int userId,
                                                 @RequestParam("serverId") int serverId){
        UserDTO user = userService.getUser(userId);
        ServerDTO server = serverService.getServer(serverId);
        if(Objects.isNull(user) || Objects.isNull(server)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(user.isDeactivated()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        ClusterDTOWithoutUsers cluster = user.getClusters().stream().filter(c -> c.getServers().contains(server)).findFirst().orElse(null);
        if(Objects.isNull(cluster)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        command.setUser(user);
        if(serverService.sendCommand(server, command)) {
            return new ResponseEntity<>(commandService.createCommand(command), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/sent")
    public ResponseEntity<Iterable<CommandDTO>> getSentCommands(){
        return new ResponseEntity<>(commandService.getCommands(), HttpStatus.OK);
    }
}

package com.java.training.finalexercise.repository;

import com.java.training.finalexercise.model.command.Command;
import org.springframework.data.repository.CrudRepository;

public interface CommandRepository extends CrudRepository<Command, Integer> {
}

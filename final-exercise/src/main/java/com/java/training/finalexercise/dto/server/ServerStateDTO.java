package com.java.training.finalexercise.dto.server;


import com.java.training.finalexercise.model.server.State;

import java.util.Date;
import java.util.Objects;

public class ServerStateDTO {
    private int id;
    private Date timeStamp = new Date();
    private State state = State.SHUTDOWN;
    private String process;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerStateDTO that = (ServerStateDTO) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public static class Builder{
        private int id;
        private Date timeStamp = new Date();
        private State serverState = State.SHUTDOWN;
        private String process;

        private Builder(){}

        public static Builder aServerStateDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withTimeStamp(Date timeStamp){
            this.timeStamp = timeStamp;
            return this;
        }

        public Builder withState(State state){
            this.serverState = state;
            return this;
        }

        public Builder withProcess(String process){
            this.process = process;
            return this;
        }

        public ServerStateDTO build(){
            ServerStateDTO serverState = new ServerStateDTO();
            serverState.setId(this.id);
            serverState.setTimeStamp(this.timeStamp);
            serverState.setState(this.serverState);
            serverState.setProcess(this.process);

            return serverState;
        }
    }
}

package com.java.training.finalexercise.dto.server;

import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ServerDTO {
    private int id;

    private String logicalName;

    private String ipAddress;

    private String cpu;

    private String memory;

    private ServerStateDTO actualState = new ServerStateDTO();

    private List<ServerStateDTO> previousStates = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogicalName() {
        return logicalName;
    }

    public void setLogicalName(String logicalName) {
        this.logicalName = logicalName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public ServerStateDTO getActualState() {
        return actualState;
    }

    public void setActualState(ServerStateDTO actualState) {
        this.actualState = actualState;
    }

    public List<ServerStateDTO> getPreviousStates() {
        return previousStates;
    }

    public void setPreviousStates(List<ServerStateDTO> previousStates) {
        this.previousStates = previousStates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerDTO serverDTO = (ServerDTO) o;
        return id == serverDTO.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public static class Builder{
        private int id;

        private String logicalName;

        @Pattern(regexp = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
        private String ipAddress;

        private String cpu;

        private String memory;

        private ServerStateDTO actualState = new ServerStateDTO();

        private List<ServerStateDTO> previousStates = new ArrayList<>();

        private Builder(){}

        public static Builder aServerDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withLogicalName(String logicalName){
            this.logicalName = logicalName;
            return this;
        }

        public Builder withIPAdress(String ipAddress){
            this.ipAddress = ipAddress;
            return this;
        }

        public Builder withCPU(String cpu){
            this.cpu = cpu;
            return this;
        }

        public Builder withMemory(String memory){
            this.memory = memory;
            return this;
        }

        public Builder withActualState(ServerStateDTO serverState){
            this.actualState = serverState;
            return this;
        }

        public Builder withPreviousStates(List<ServerStateDTO> previousStates){
            this.previousStates = previousStates;
            return this;
        }

        public ServerDTO build(){
            ServerDTO server = new ServerDTO();
            server.setId(this.id);
            server.setLogicalName(this.logicalName);
            server.setIpAddress(this.ipAddress);
            server.setCpu(this.cpu);
            server.setMemory(this.memory);
            server.setActualState(this.actualState);
            server.setPreviousStates(this.previousStates);

            return server;
        }
    }
}

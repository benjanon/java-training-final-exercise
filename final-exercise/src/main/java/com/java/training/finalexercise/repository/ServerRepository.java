package com.java.training.finalexercise.repository;

import com.java.training.finalexercise.model.server.Server;
import org.springframework.data.repository.CrudRepository;

public interface ServerRepository extends CrudRepository<Server, Integer> {
}

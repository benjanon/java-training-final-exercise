package com.java.training.finalexercise.dto.user;

import com.java.training.finalexercise.dto.cluster.ClusterDTOWithoutUsers;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class UserDTO {
    private int id;
    private String name;
    private Set<ClusterDTOWithoutUsers> clusters = new HashSet<>();
    private boolean isDeactivated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ClusterDTOWithoutUsers> getClusters() {
        return clusters;
    }

    public void setClusters(Set<ClusterDTOWithoutUsers> clusters) {
        this.clusters = clusters;
    }

    public boolean isDeactivated() {
        return isDeactivated;
    }

    public void setDeactivated(boolean deactivated) {
        isDeactivated = deactivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return id == userDTO.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public static class Builder{
        private int id;
        private String name;
        private Set<ClusterDTOWithoutUsers> clusters = new HashSet<>();
        private boolean isDeactivated;

        private Builder(){}

        public static Builder aUserDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withClusters(Set<ClusterDTOWithoutUsers> clusters){
            this.clusters = clusters;
            return this;
        }

        public Builder withDeactivated(boolean isDeactivated){
            this.isDeactivated = isDeactivated;
            return this;
        }

        public UserDTO build(){
            UserDTO user = new UserDTO();
            user.setId(this.id);
            user.setName(this.name);
            user.setDeactivated(this.isDeactivated);
            user.setClusters(this.clusters);

            return user;
        }
    }
}

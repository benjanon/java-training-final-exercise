package com.java.training.finalexercise.model.server;

public enum State {
    SHUTDOWN,
    WAITING,
    RUNNING
}

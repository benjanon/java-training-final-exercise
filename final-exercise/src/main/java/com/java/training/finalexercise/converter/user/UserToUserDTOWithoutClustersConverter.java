package com.java.training.finalexercise.converter.user;

import com.java.training.finalexercise.dto.user.UserDTOWithoutClusters;
import com.java.training.finalexercise.model.user.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDTOWithoutClustersConverter implements Converter<User, UserDTOWithoutClusters> {

    @Override
    public UserDTOWithoutClusters convert(User user) {
        return UserDTOWithoutClusters.Builder.aUserDTO()
                .withId(user.getId())
                .withName(user.getName())
                .withDeactivated(user.isDeactivated())
                .build();
    }
}

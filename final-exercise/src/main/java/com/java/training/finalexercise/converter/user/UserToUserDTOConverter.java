package com.java.training.finalexercise.converter.user;

import com.java.training.finalexercise.converter.cluster.ClusterToClusterDTOWithoutUserConverter;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserToUserDTOConverter implements Converter<User, UserDTO> {

    private ClusterToClusterDTOWithoutUserConverter clusterToClusterDTOWithoutUserConverter;

    @Autowired
    public UserToUserDTOConverter(ClusterToClusterDTOWithoutUserConverter clusterToClusterDTOWithoutUserConverter) {
        this.clusterToClusterDTOWithoutUserConverter = clusterToClusterDTOWithoutUserConverter;
    }


    @Override
    public UserDTO convert(User user) {
        return UserDTO.Builder.aUserDTO()
                .withId(user.getId())
                .withDeactivated(user.isDeactivated())
                .withName(user.getName())
                .withClusters(user.getClusters().stream().map(clusterToClusterDTOWithoutUserConverter::convert).collect(Collectors.toSet()))
                .build();
    }
}

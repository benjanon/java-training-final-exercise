package com.java.training.finalexercise.repository;

import com.java.training.finalexercise.model.user.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}

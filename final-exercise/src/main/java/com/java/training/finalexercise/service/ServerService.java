package com.java.training.finalexercise.service;

import com.java.training.finalexercise.converter.command.CommandDtoToCommandConverter;
import com.java.training.finalexercise.converter.server.ServerDTOToServerConverter;
import com.java.training.finalexercise.converter.server.ServerToServerDTOConverter;
import com.java.training.finalexercise.dto.command.CommandDTO;
import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.dto.server.ServerStateDTO;
import com.java.training.finalexercise.model.command.Command;
import com.java.training.finalexercise.model.server.Server;
import com.java.training.finalexercise.model.server.ServerState;
import com.java.training.finalexercise.model.server.State;
import com.java.training.finalexercise.repository.ServerRepository;
import com.java.training.finalexercise.repository.ServerStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ServerService {

    private ServerRepository serverRepository;
    private ServerStateRepository serverStateRepository;
    private UtilsService utilsService;
    private ServerToServerDTOConverter serverToServerDTOConverter;
    private ServerDTOToServerConverter serverDTOToServerConverter;
    private CommandDtoToCommandConverter commandDtoToCommandConverter;

    @Autowired
    public ServerService(ServerRepository serverRepository,
                         ServerStateRepository serverStateRepository,
                         UtilsService utilsService,
                         ServerToServerDTOConverter serverToServerDTOConverter,
                         ServerDTOToServerConverter serverDTOToServerConverter,
                         CommandDtoToCommandConverter commandDtoToCommandConverter) {
        this.serverRepository = serverRepository;
        this.serverStateRepository = serverStateRepository;
        this.utilsService = utilsService;
        this.serverToServerDTOConverter = serverToServerDTOConverter;
        this.serverDTOToServerConverter = serverDTOToServerConverter;
        this.commandDtoToCommandConverter = commandDtoToCommandConverter;
    }

    public ServerDTO createServer(ServerDTO serverDTO){
        Server server = serverDTOToServerConverter.convert(serverDTO);
        serverStateRepository.save(server.getActualState());
        return serverToServerDTOConverter.convert(serverRepository.save(server));
    }

    public ServerDTO updateServer(ServerDTO serverDTO){
        return serverToServerDTOConverter.convert(serverRepository.save(serverDTOToServerConverter.convert(serverDTO)));
    }

    public ServerDTO getServer(int id){
        Server server = serverRepository.findById(id).orElse(null);
        return Objects.isNull(server) ? null : serverToServerDTOConverter.convert(server);
    }

    public Iterable<ServerDTO> getServers(){
        return utilsService.toList(serverRepository.findAll()).stream()
                .map(serverToServerDTOConverter::convert)
                .collect(Collectors.toList());
    }

    public boolean sendCommand(ServerDTO serverDTO, CommandDTO commandDTO){
        Server server = serverDTOToServerConverter.convert(serverDTO);
        Command command = commandDtoToCommandConverter.convert(commandDTO);

        ServerState previousState = server.getActualState();
        ServerState newState = new ServerState();

        switch (command.getCommandType()){
            case STARTUP:
                if(previousState.getState() != State.SHUTDOWN){
                    return false;
                }
                newState.setState(State.WAITING);
                break;
            case SHOTDOWN:
                if(previousState.getState() != State.WAITING){
                    return false;
                }
                newState.setState(State.SHUTDOWN);
                break;
            case RUNPROCESS:
                if(previousState.getState() != State.WAITING){
                    return false;
                }
                newState.setState(State.RUNNING);
                break;
            case STOPPROCESS:
                if(previousState.getState() != State.RUNNING){
                    return false;
                }
                newState.setState(State.WAITING);
                break;
            default:
                return false;
        }

        newState.setTimeStamp(commandDTO.getTimeStamp());
        newState.setProcess(commandDTO.getProcess());

        server.getPreviousStates().add(previousState);
        server.setActualState(newState);

        this.serverStateRepository.save(newState);
        this.serverRepository.save(server);

        return true;
    }
}

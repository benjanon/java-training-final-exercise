package com.java.training.finalexercise.controller;

import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.dto.server.ServerStateDTO;
import com.java.training.finalexercise.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/server")
public class ServerController {

    private ServerService serverService;

    @Autowired
    public ServerController(ServerService serverService) {
        this.serverService = serverService;
    }

    @PostMapping("/create")
    public ServerDTO createServer(@RequestBody ServerDTO server){
        return serverService.createServer(server);
    }

    @GetMapping("/get")
    public ServerDTO getServer(@RequestParam("serverId") int serverId){
        return serverService.getServer(serverId);
    }

    @GetMapping("/all")
    public Iterable<ServerDTO> getAllServers(){
        return serverService.getServers();
    }

    @GetMapping("/states/previous")
    public ResponseEntity<List<ServerStateDTO>> getServerPreviousStates(@RequestParam("serverId") int serverId){
        ServerDTO server = serverService.getServer(serverId);
        if(Objects.isNull(server)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(server.getPreviousStates(), HttpStatus.OK);
    }
}

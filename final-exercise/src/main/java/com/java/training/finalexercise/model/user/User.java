package com.java.training.finalexercise.model.user;

import com.java.training.finalexercise.model.cluster.Cluster;

import javax.persistence.*;
import java.util.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    @ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
    private Set<Cluster> clusters = new HashSet<>();

    private boolean isDeactivated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(Set<Cluster> clusters) {
        this.clusters = clusters;
    }

    public boolean isDeactivated() {
        return isDeactivated;
    }

    public void setDeactivated(boolean deactivated) {
        isDeactivated = deactivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static class Builder{
        private int id;
        private String name;
        private Set<Cluster> clusters = new HashSet<>();
        private boolean isDeactivated;

        private Builder(){}

        public static Builder aUser(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withClusters(Set<Cluster> clusters){
            this.clusters = clusters;
            return this;
        }

        public Builder withDeactivated(boolean isDactivated){
            this.isDeactivated = isDactivated;
            return this;
        }

        public User build(){
            User user = new User();
            user.setId(this.id);
            user.setName(this.name);
            user.setDeactivated(this.isDeactivated);
            user.setClusters(this.clusters);

            return user;
        }
    }
}

package com.java.training.finalexercise.model.command;

import com.java.training.finalexercise.model.user.User;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Command {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private Date timeStamp = new Date();

    @OneToOne
    private User user;

    private CommandType commandType;

    private String process;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public static class Builder{
        private int id;

        private Date timeStamp = new Date();

        private User user;

        private CommandType commandType;

        private String process;

        private Builder(){}

        public static Builder aCommand(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withTimeStamp(Date timeStamp){
            this.timeStamp = timeStamp;
            return this;
        }

        public Builder withUser(User user){
            this.user = user;
            return this;
        }

        public Builder withCommandType(CommandType commandType){
            this.commandType = commandType;
            return this;
        }

        public Builder withProcess(String process){
            this.process = process;
            return this;
        }


        public Command build(){
            Command command = new Command();
            command.setId(this.id);
            command.setTimeStamp(this.timeStamp);
            command.setUser(this.user);
            command.setCommandType(this.commandType);
            command.setProcess(this.process);

            return command;
        }
    }
}

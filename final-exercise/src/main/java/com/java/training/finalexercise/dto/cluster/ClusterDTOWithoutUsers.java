package com.java.training.finalexercise.dto.cluster;

import com.java.training.finalexercise.dto.server.ServerDTO;

import java.util.ArrayList;
import java.util.List;

public class ClusterDTOWithoutUsers {
    private int id;
    private String name;

    private List<ServerDTO> servers = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServerDTO> getServers() {
        return servers;
    }

    public void setServers(List<ServerDTO> servers) {
        this.servers = servers;
    }

    public static class Builder{
        private int id;
        private String name;

        private List<ServerDTO> servers = new ArrayList<>();

        private Builder(){}

        public static Builder aClusterDTO(){
            return new Builder();
        }

        public Builder withId(int id){
            this.id = id;
            return this;
        }

        public Builder withName(String name){
            this.name = name;
            return this;
        }

        public Builder withServers(List<ServerDTO> servers){
            this.servers = servers;
            return this;
        }

        public ClusterDTOWithoutUsers build(){
            ClusterDTOWithoutUsers cluster = new ClusterDTOWithoutUsers();
            cluster.setId(this.id);
            cluster.setName(this.name);
            cluster.setServers(this.servers);

            return cluster;
        }
    }
}

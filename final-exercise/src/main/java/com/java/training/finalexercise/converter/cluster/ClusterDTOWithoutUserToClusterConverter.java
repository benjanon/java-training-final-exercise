package com.java.training.finalexercise.converter.cluster;

import com.java.training.finalexercise.converter.server.ServerDTOToServerConverter;
import com.java.training.finalexercise.dto.cluster.ClusterDTOWithoutUsers;
import com.java.training.finalexercise.model.cluster.Cluster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ClusterDTOWithoutUserToClusterConverter implements Converter<ClusterDTOWithoutUsers, Cluster> {

    private ServerDTOToServerConverter serverDTOToServerConverter;

    @Autowired
    public ClusterDTOWithoutUserToClusterConverter(ServerDTOToServerConverter serverDTOToServerConverter) {
        this.serverDTOToServerConverter = serverDTOToServerConverter;
    }

    @Override
    public Cluster convert(ClusterDTOWithoutUsers clusterDTOWithoutUsers) {
        return Cluster.Builder.aCluster()
                .withId(clusterDTOWithoutUsers.getId())
                .withName(clusterDTOWithoutUsers.getName())
                .withServers(
                        clusterDTOWithoutUsers.getServers().stream()
                        .map(serverDTOToServerConverter::convert)
                        .collect(Collectors.toList())
                )
                .build();
    }
}

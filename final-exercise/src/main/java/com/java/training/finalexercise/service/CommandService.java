package com.java.training.finalexercise.service;

import com.java.training.finalexercise.converter.command.CommandDtoToCommandConverter;
import com.java.training.finalexercise.converter.command.CommandToCommandDTOConverter;
import com.java.training.finalexercise.dto.command.CommandDTO;
import com.java.training.finalexercise.model.command.Command;
import com.java.training.finalexercise.repository.CommandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommandService {

    private CommandRepository commandRepository;
    private CommandToCommandDTOConverter commandToCommandDTOConverter;
    private CommandDtoToCommandConverter commandDtoToCommandConverter;
    private UtilsService utilsService;

    @Autowired
    public CommandService(CommandRepository commandRepository,
                          CommandToCommandDTOConverter commandToCommandDTOConverter,
                          CommandDtoToCommandConverter commandDtoToCommandConverter,
                          UtilsService utilsService) {
        this.commandRepository = commandRepository;
        this.commandToCommandDTOConverter = commandToCommandDTOConverter;
        this.commandDtoToCommandConverter = commandDtoToCommandConverter;
        this.utilsService = utilsService;
    }

    public CommandDTO createCommand(CommandDTO command){
        return commandToCommandDTOConverter.convert(commandRepository.save(commandDtoToCommandConverter.convert(command)));
    }

    public CommandDTO updateCommand(CommandDTO command){
        return commandToCommandDTOConverter.convert(commandRepository.save(commandDtoToCommandConverter.convert(command)));
    }

    public CommandDTO getCommand(int id){
        Command c = commandRepository.findById(id).orElse(null);
        return Objects.isNull(c) ? null : commandToCommandDTOConverter.convert(c);
    }

    public Iterable<CommandDTO> getCommands(){
        return utilsService.toList(commandRepository.findAll()).stream()
                .map(commandToCommandDTOConverter::convert)
                .collect(Collectors.toList());
    }
}

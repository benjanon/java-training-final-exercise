package com.java.training.finalexercise.service;

import com.java.training.finalexercise.converter.user.UserDTOToUserConverter;
import com.java.training.finalexercise.converter.user.UserToUserDTOConverter;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.user.User;
import com.java.training.finalexercise.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository userRepository;
    private UserToUserDTOConverter userToUserDTOConverter;
    private UserDTOToUserConverter userDTOToUserConverter;
    private UtilsService utilsService;

    @Autowired
    public UserService(UserRepository userRepository,
                       UserToUserDTOConverter userToUserDTOConverter,
                       UserDTOToUserConverter userDTOToUserConverter,
                       UtilsService utilsService) {
        this.userRepository = userRepository;
        this.userToUserDTOConverter = userToUserDTOConverter;
        this.userDTOToUserConverter = userDTOToUserConverter;
        this.utilsService = utilsService;
    }

    public UserDTO createUser(UserDTO user){
        return userToUserDTOConverter.convert(userRepository.save(userDTOToUserConverter.convert(user)));
    }

    public UserDTO updateUser(UserDTO user){
        return userToUserDTOConverter.convert(userRepository.save(userDTOToUserConverter.convert(user)));
    }

    public UserDTO getUser(int id){
        User user = userRepository.findById(id).orElse(null);
        return Objects.isNull(user) ? null : userToUserDTOConverter.convert(user);
    }

    public List<UserDTO> getUsers(){
        return utilsService.toList(userRepository.findAll()).stream()
                .map(userToUserDTOConverter::convert)
                .collect(Collectors.toList());
    }
}

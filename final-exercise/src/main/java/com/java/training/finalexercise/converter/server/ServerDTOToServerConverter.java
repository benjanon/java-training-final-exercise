package com.java.training.finalexercise.converter.server;

import com.java.training.finalexercise.converter.serverstate.ServerStateDTOToServerStateConverter;
import com.java.training.finalexercise.dto.server.ServerDTO;
import com.java.training.finalexercise.model.server.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ServerDTOToServerConverter implements Converter<ServerDTO, Server> {

    private ServerStateDTOToServerStateConverter serverStateDTOToServerStateConverter;

    @Autowired
    public ServerDTOToServerConverter(ServerStateDTOToServerStateConverter serverStateDTOToServerStateConverter) {
        this.serverStateDTOToServerStateConverter = serverStateDTOToServerStateConverter;
    }

    @Override
    public Server convert(ServerDTO serverDTO) {
        return Server.Builder.aServer()
                .withId(serverDTO.getId())
                .withIPAdress(serverDTO.getIpAddress())
                .withCPU(serverDTO.getCpu())
                .withMemory(serverDTO.getMemory())
                .withLogicalName(serverDTO.getLogicalName())
                .withActualState(serverStateDTOToServerStateConverter.convert(serverDTO.getActualState()))
                .withPreviousStates(serverDTO.getPreviousStates().stream().map(serverStateDTOToServerStateConverter::convert).collect(Collectors.toList()))
                .build();
    }
}

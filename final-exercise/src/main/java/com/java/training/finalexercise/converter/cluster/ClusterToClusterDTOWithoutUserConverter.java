package com.java.training.finalexercise.converter.cluster;

import com.java.training.finalexercise.converter.server.ServerToServerDTOConverter;
import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.dto.cluster.ClusterDTOWithoutUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ClusterToClusterDTOWithoutUserConverter implements Converter<Cluster, ClusterDTOWithoutUsers> {

    private ServerToServerDTOConverter serverToServerDTOConverter;

    @Autowired
    public ClusterToClusterDTOWithoutUserConverter(ServerToServerDTOConverter serverToServerDTOConverter) {
        this.serverToServerDTOConverter = serverToServerDTOConverter;
    }

    @Override
    public ClusterDTOWithoutUsers convert(Cluster cluster) {
        return ClusterDTOWithoutUsers.Builder.aClusterDTO()
                .withId(cluster.getId())
                .withName(cluster.getName())
                .withServers(
                        cluster.getServers().stream()
                                .map(serverToServerDTOConverter::convert)
                                .collect(Collectors.toList())
                )
                .build();
    }
}

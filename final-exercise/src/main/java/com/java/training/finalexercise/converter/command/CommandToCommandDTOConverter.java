package com.java.training.finalexercise.converter.command;

import com.java.training.finalexercise.converter.user.UserToUserDTOConverter;
import com.java.training.finalexercise.model.command.Command;
import com.java.training.finalexercise.dto.command.CommandDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CommandToCommandDTOConverter implements Converter<Command, CommandDTO> {

    private UserToUserDTOConverter userToUserDTOConverter;

    @Autowired
    public CommandToCommandDTOConverter(UserToUserDTOConverter userToUserDTOConverter) {
        this.userToUserDTOConverter = userToUserDTOConverter;
    }

    @Override
    public CommandDTO convert(Command command) {
        return CommandDTO.Builder.aCommandDTO().withId(command.getId())
                .withCommandType(command.getCommandType())
                .withProcess(command.getProcess())
                .withTimeStamp(command.getTimeStamp())
                .withUser(userToUserDTOConverter.convert(command.getUser()))
                .build();
    }
}

package com.java.training.finalexercise.converter.user;

import com.java.training.finalexercise.converter.cluster.ClusterDTOWithoutUserToClusterConverter;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserDTOToUserConverter implements Converter<UserDTO, User> {

    private ClusterDTOWithoutUserToClusterConverter clusterDTOWithoutUserToClusterConverter;

    @Autowired
    public UserDTOToUserConverter(ClusterDTOWithoutUserToClusterConverter clusterDTOWithoutUserToClusterConverter) {
        this.clusterDTOWithoutUserToClusterConverter = clusterDTOWithoutUserToClusterConverter;
    }

    @Override
    public User convert(UserDTO userDTO) {
        return User.Builder.aUser()
                .withId(userDTO.getId())
                .withDeactivated(userDTO.isDeactivated())
                .withName(userDTO.getName())
                .withClusters(userDTO.getClusters().stream().map(clusterDTOWithoutUserToClusterConverter::convert).collect(Collectors.toSet()))
                .build();
    }
}

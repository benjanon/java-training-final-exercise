package com.java.training.finalexercise.controller;

import com.java.training.finalexercise.dto.cluster.ClusterDTO;
import com.java.training.finalexercise.dto.cluster.ClusterDTOWithoutUsers;
import com.java.training.finalexercise.dto.user.UserDTO;
import com.java.training.finalexercise.model.cluster.Cluster;
import com.java.training.finalexercise.model.user.User;
import com.java.training.finalexercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO user){
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.OK);
    }

    @GetMapping("/deactivate")
    public ResponseEntity<UserDTO> deactivateUser(@RequestParam("userId") int userId){
        UserDTO user = userService.getUser(userId);
        if(Objects.isNull(user)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        user.setDeactivated(true);
        return new ResponseEntity<>(userService.updateUser(user), HttpStatus.OK);
    }

    @GetMapping("/user")
    public ResponseEntity<UserDTO> getUser(@RequestParam("userId") int userId){
        return new ResponseEntity<>(userService.getUser(userId), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Iterable<UserDTO>> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @GetMapping("/clusters")
    public ResponseEntity<Set<ClusterDTOWithoutUsers>> getClustersOfUser(@RequestParam("userId") int userId){
        UserDTO user = userService.getUser(userId);
        if(Objects.isNull(user)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user.getClusters(), HttpStatus.OK);
    }
}
